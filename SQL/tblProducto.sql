-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 23, 2020 at 02:17 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ev2db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblProducto`
--

CREATE TABLE `tblProducto` (
  `codigoProd` int(11) NOT NULL,
  `nombreProd` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `precioProd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Dumping data for table `tblProducto`
--

INSERT INTO `tblProducto` (`codigoProd`, `nombreProd`, `precioProd`) VALUES
(681, 'Pantalon', 6000),
(682, 'Camisa', 5000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblProducto`
--
ALTER TABLE `tblProducto`
  ADD PRIMARY KEY (`codigoProd`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblProducto`
--
ALTER TABLE `tblProducto`
  MODIFY `codigoProd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=683;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
