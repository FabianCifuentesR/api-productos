const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const app = express();

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'ev2db'
});

connection.connect(err => {
    if(err) throw err;
    console.log('Conectado!!!');
});

app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'));
});

app.get('/products', (req, res) => {
    const sql = 'SELECT * FROM tblProducto';
    connection.query(sql,(err, result) => {
        if(err) throw(err);
        if(result.length > 0){
            res.json(result);
        }else{
            res.send('Sin Resultados')
        }
    });
});
app.get('/products/:codigoProd', (req, res) => {
    const {codigoProd} = req.params;
    const sql = 'SELECT * FROM tblProducto WHERE ?';
    connection.query(sql, {codigoProd}, (err, result) => {
        if(err) throw(err);
        if(result.length > 0){
            res.json(result);
        }else{
            res.send('Sin Resultados')
        }
    });
});
app.post('/add', (req, res) => {
    const sql = 'INSERT INTO tblProducto SET ?';
    const productObj = {
        nombreProd: req.body.nombreProd,
        precioProd: req.body.precioProd
    }
    connection.query(sql, productObj, err => {
        if (err) throw err;
        res.send('Producto Agregado');
    });
});

app.put('/update/:codigoProd', (req, res) => {
    const {codigoProd} = req.params;
    const {nombreProd, precioProd} = req.body;
    const sql = `UPDATE tblProducto SET nombreProd = '${nombreProd}', precioProd = ${precioProd} WHERE codigoProd = ${codigoProd}`;
    connection.query(sql, err => {
        if (err) throw err;
        res.send('Producto Actualizado');
    });
});

app.delete('/delete/:codigoProd', (req, res) => {
    const {codigoProd} = req.params;
    const sql = `DELETE FROM tblProducto WHERE codigoProd = ${codigoProd}`;
    connection.query(sql, err => {
        if (err) throw err;
        res.send('Producto Eliminado');
    });
})